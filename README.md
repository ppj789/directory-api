# Directory API

## Description
Docker contained, RESTful interface on port 8080 which allows a client application to obtain the full
directory listing of a given path in the container.\
Gathering the full path, file size and attribute information in the result and caters for a directory size of at most 1 000 000 entries.

Uses Javalin which is localhost:8080\
Prometheus uses port 8081\
Prometheus access on localhost:9090

## Installation
Requires Docker inclusive of Docker Compose and Maven 

In the project folder\
mvn package -f ./my-app\
docker-compose up


## Usage

localhost:8080/ swagger ui\
localhost:8080/red redoc 

logs only on terminal/cmd

API
localhost:8080/directory gets all files up to 1000000 entries\
localhost:8080/directory/{path} gets all files and directories for the path, path needs to be url encoded\
https://www.urlencoder.org/ 


Promethus 
localhost:9090/ \
localhost:8081 used by promethus

## Reponse
200 response will return
[{"file":filePath,"dataMap":Map of 21 attributes}]

## Roadmap
Api calls, such as changing a File name, search, adding and deleting a file\
Figure out encoding, maybe api with body with file path\
Logging to a file\
More monitoring checks 
