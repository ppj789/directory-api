package app;

import java.io.File;
import java.util.TreeMap;

public class DirectoryFile {
    
	public File file;
	public TreeMap<String, String> dataMap;
	
	
    public DirectoryFile(File file, TreeMap<String, String> dataMap) {
    	this.file = file;
		this.dataMap = dataMap;
	}
    
}