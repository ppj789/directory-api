package app;

import static io.javalin.apibuilder.ApiBuilder.*;

import java.io.IOException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import app.directory.DirectoryController;
import app.health.HealthController;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.prometheus.client.exporter.HTTPServer;
import io.swagger.v3.oas.models.info.Info;
 
public class Main {
	
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

	
    public static void main(String[] args) {
    	
    	StatisticsHandler statisticsHandler = new StatisticsHandler();
    	QueuedThreadPool queuedThreadPool = new QueuedThreadPool(200, 8, 60_000);
    	
    	Javalin app = Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
            config.requestLogger((ctx, executionTimeMs) -> {
                logger.info("{} {} {} {} \"{}\" {}",
                        ctx.method(),  ctx.url(), ctx.req.getRemoteHost(),
                        ctx.res.getStatus(), ctx.userAgent(), executionTimeMs.longValue());
            });
            
            config.server(() -> {
                Server server = new Server(queuedThreadPool);
                server.setHandler(statisticsHandler);
                return server;
            });
        }).start(8080);
    	
    	try {
    	
    		initializePrometheus(statisticsHandler, queuedThreadPool);
		} catch (Exception e) {
			Gson gson = new Gson();
		    logger.error(gson.toJson(e));
		}
    	
    	
    	//Directory
        app.routes(() -> {
            path("directory", () -> {
                get(DirectoryController::listFiles);
                path("{filePath}", () -> {
                	get(DirectoryController::filesInPath);
                });
            });
            path("health", () -> {
            	path("make", () -> {
                	post(HealthController::makeMFiles);
            	});
            	path("spawn", () -> {
            		get(HealthController::spawn);
            	});
            });
        });
    }
    

	private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("Directory API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("my-app")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", ErrorResponse.class);
                    doc.json("200", DirectoryFile[].class);
                });
                
        return new OpenApiPlugin(options);
    }
	
	private static void initializePrometheus(StatisticsHandler statisticsHandler, QueuedThreadPool queuedThreadPool) throws IOException {
	    StatisticsHandlerCollector.initialize(statisticsHandler); // collector is included in source code
	    QueuedThreadPoolCollector.initialize(queuedThreadPool); // collector is included in source code
	    HTTPServer prometheusServer = new HTTPServer(8081);
	    
	    logger.info("Prometheus is listening on: http://localhost:8081");
	}
}