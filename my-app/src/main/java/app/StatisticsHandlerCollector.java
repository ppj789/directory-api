package app;

import io.prometheus.client.Collector;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.jetty.server.handler.StatisticsHandler;

public class StatisticsHandlerCollector extends Collector {

    private final StatisticsHandler statisticsHandler;
    private static final List<String> EMPTY_LIST = new ArrayList<>();

    private StatisticsHandlerCollector(StatisticsHandler statisticsHandler) {
        this.statisticsHandler = statisticsHandler;
    }

    public static void initialize(StatisticsHandler statisticsHandler) {
        new StatisticsHandlerCollector(statisticsHandler).register();
    }

    @Override
    public List<MetricFamilySamples> collect() {
        return Arrays.asList(
            buildCounter("app_requests_total", "Number of requests", statisticsHandler.getRequests()),
            buildGauge("app_requests_active", "Number of requests currently active", statisticsHandler.getRequestsActive()),
            buildGauge("app_requests_active_max", "Maximum number of requests that have been active at once", statisticsHandler.getRequestsActiveMax()),
            buildGauge("app_request_time_max_seconds", "Maximum time spent handling requests", statisticsHandler.getRequestTimeMax() / 1000.0),
            buildCounter("app_request_time_seconds_total", "Total time spent in all request handling", statisticsHandler.getRequestTimeTotal() / 1000.0),
            buildCounter("app_dispatched_total", "Number of dispatches", statisticsHandler.getDispatched()),
            buildGauge("app_dispatched_active", "Number of dispatches currently active", statisticsHandler.getDispatchedActive()),
            buildGauge("app_dispatched_active_max", "Maximum number of active dispatches being handled", statisticsHandler.getDispatchedActiveMax()),
            buildGauge("app_dispatched_time_max", "Maximum time spent in dispatch handling", statisticsHandler.getDispatchedTimeMax()),
            buildCounter("app_dispatched_time_seconds_total", "Total time spent in dispatch handling", statisticsHandler.getDispatchedTimeTotal() / 1000.0),
            buildCounter("app_async_requests_total", "Total number of async requests", statisticsHandler.getAsyncRequests()),
            buildGauge("app_async_requests_waiting", "Currently waiting async requests", statisticsHandler.getAsyncRequestsWaiting()),
            buildGauge("app_async_requests_waiting_max", "Maximum number of waiting async requests", statisticsHandler.getAsyncRequestsWaitingMax()),
            buildCounter("app_async_dispatches_total", "Number of requested that have been asynchronously dispatched", statisticsHandler.getAsyncDispatches()),
            buildCounter("app_expires_total", "Number of async requests requests that have expired", statisticsHandler.getExpires()),
            buildStatusCounter(),
            buildGauge("app_stats_seconds", "Time in seconds stats have been collected for", statisticsHandler.getStatsOnMs() / 1000.0),
            buildCounter("app_responses_bytes_total", "Total number of bytes across all responses", statisticsHandler.getResponsesBytesTotal())
        );
    }

    private static MetricFamilySamples buildGauge(String name, String help, double value) {
        return new MetricFamilySamples(
            name,
            Type.GAUGE,
            help,
            Collections.singletonList(new MetricFamilySamples.Sample(name, EMPTY_LIST, EMPTY_LIST, value))
        );
    }

    private static MetricFamilySamples buildCounter(String name, String help, double value) {
        return new MetricFamilySamples(
            name,
            Type.COUNTER,
            help,
            Collections.singletonList(new MetricFamilySamples.Sample(name, EMPTY_LIST, EMPTY_LIST, value))
        );
    }

    private MetricFamilySamples buildStatusCounter() {
        String name = "app_responses_total";
        return new MetricFamilySamples(
            name,
            Type.COUNTER,
            "Number of requests with response status",
            Arrays.asList(
                buildStatusSample(name, "1xx", statisticsHandler.getResponses1xx()),
                buildStatusSample(name, "2xx", statisticsHandler.getResponses2xx()),
                buildStatusSample(name, "3xx", statisticsHandler.getResponses3xx()),
                buildStatusSample(name, "4xx", statisticsHandler.getResponses4xx()),
                buildStatusSample(name, "5xx", statisticsHandler.getResponses5xx())
            )
        );
    }

    private static MetricFamilySamples.Sample buildStatusSample(String name, String status, double value) {
        return new MetricFamilySamples.Sample(
            name,
            Collections.singletonList("code"),
            Collections.singletonList(status),
            value
        );
    }

}