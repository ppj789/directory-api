package app.directory;


import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.*;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.DirectoryFile;
import app.ErrorResponse;

public class DirectoryController {
	
	private static final Logger logger = LoggerFactory.getLogger(DirectoryController.class);
    
	@OpenApi(
            summary = "Show every file on container",
            operationId = "listFiles",
            path = "/directory",
            method = HttpMethod.GET,
            tags = {"Directory"},
            responses = {
            		@OpenApiResponse(status = "200", content = {@OpenApiContent(from = DirectoryFile[].class)}),
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "500", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void listFiles(Context ctx) {
    	try {
			ctx.json(DirectoryService.listAllFiles());
		} catch (Exception e) {
			logger.error("Sending back error 500.", e.getStackTrace().toString(), ctx, e);
			ctx.status(500);
		}
    }
    
    @OpenApi(
    		 summary = "Get files in given path",
             operationId = "filesInPath",
             path = "/directory/:filePath",
             method = HttpMethod.GET,
             pathParams = {@OpenApiParam(name = "filePath", type = String.class, description = "Path of folder")},
             tags = {"Directory"},          
             responses = {
             		 @OpenApiResponse(status = "200", content = {@OpenApiContent(from = DirectoryFile[].class)}),
                     @OpenApiResponse(status = "204"),
                     @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                     @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                     @OpenApiResponse(status = "500", content = {@OpenApiContent(from = ErrorResponse.class)})
             }

    )
    public static void filesInPath(Context ctx) {
    	try {
			ctx.json(DirectoryService.listOfFilesInPath(validPathParamFilePath(ctx)));
		} catch (Exception e) {
			logger.error("Sending back error 500", e.getStackTrace().toString(), ctx, e);
			ctx.status(500);
		}
    }
    
    
    
    private static String validPathParamFilePath(Context ctx) {
		return ctx.pathParamAsClass("filePath", String.class).check(filePath -> filePath.equals("root") || Files.exists(Paths.get(filePath)),"Could not find file with path, try root").get();
    }
 
}
