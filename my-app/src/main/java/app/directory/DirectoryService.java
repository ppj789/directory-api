package app.directory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.TreeMap;

import app.DirectoryFile;

public class DirectoryService {
	
	 
	public static List<DirectoryFile> listRoots() throws IOException
	{
		List<DirectoryFile> fileList = new ArrayList<DirectoryFile>();
		Arrays.asList(File.listRoots()).forEach( file -> fileList.add(convertToDirectoryFile(file)));
	    return fileList;
	}
	
	public static List<DirectoryFile> listAllFiles() throws IOException
	{
		Stack<File> directories = new Stack<File>();
		directories.addAll(Arrays.asList(File.listRoots()));
		System.out.println(directories);
		List<DirectoryFile> fileList = new ArrayList<DirectoryFile>();
		
		int i = 0;

	    while(!directories.isEmpty() && i < 1000000)
	    {
	    	i++;
	    	
	    	File directory = directories.pop();
	    	
	    	if(directory == null)
	    	{
	    		continue;
	    	}
	    	else if(directory.isDirectory() && directory.listFiles() != null)
	    	{
	    		for(File file : directory.listFiles())
	    		{
	    			directories.push(file);
	    		}
	    	}
	    	else 
	    	{
	    		if(directory.exists())
	    			fileList.add(convertToDirectoryFile(directory));
	    		
	    	}
	    	
	    }
	    
	    return fileList;
	}
	
	public static List<DirectoryFile> listOfFilesInPath(String path ) throws IOException{
		List<DirectoryFile> directoryFiles = new ArrayList<DirectoryFile>();
		File[] files;
		if(path.isEmpty() || path.equals("root") )
			files = File.listRoots();
		else
			files = new File(path).listFiles();
		
		Arrays.asList(files).forEach( file -> directoryFiles.add(convertToDirectoryFile(file)));
		
		return directoryFiles;
	}
	
	public static DirectoryFile convertToDirectoryFile(File file) 
	{		
	
		//created and other values
		TreeMap<String, String> dataMap = new TreeMap<String, String>();
		for (Method method : file.getClass().getDeclaredMethods()) {
		    if (Modifier.isPublic(method.getModifiers())
		        && method.getParameterTypes().length == 0
		        && method.getReturnType() != void.class
		        && (method.getName().matches("(is|get|can|exists|last|length).*")))
		    {
		    	String value;
		    	try {
		    		value =  String.valueOf( method.invoke(file) );
				} catch (Exception e) {
					value =  e.toString();
				} 
		        dataMap.put(method.getName(), value);
		    }
		}
	
		
		return new DirectoryFile(file, dataMap);
	}
	
	
	
}
