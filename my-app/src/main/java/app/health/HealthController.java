package app.health;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.DirectoryFile;
import app.ErrorResponse;
import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

public class HealthController {
	
	private static final Logger logger = LoggerFactory.getLogger(HealthController.class);
	
	@OpenApi(
            summary = "Creates a Million files",
            operationId = "make",
            path = "/health/make",
            method = HttpMethod.POST,
            tags = {"Health"},
            responses = {
            		@OpenApiResponse(status = "200", content = {@OpenApiContent(from = DirectoryFile.class)}),
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "500", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void makeMFiles(Context ctx) {
    	try {
			ctx.json(HealthService.create(1000000));
		} catch (Exception e) {
			logger.error("Sending back error 500.", e.getStackTrace().toString(), ctx, e);
			ctx.status(500);
		}
    }
	
	
	@OpenApi(
   		 summary = "Create random requests",
   		 operationId = "spawn",
         path = "/health/spawn",
         method = HttpMethod.GET,
         tags = {"Health"},          
         responses = {
        		 @OpenApiResponse(status = "200"),
                 @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                 @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)}),
                 @OpenApiResponse(status = "500", content = {@OpenApiContent(from = ErrorResponse.class)})
         }

   )
   public static void spawn(Context ctx) {
   	try {
   			HealthService.spawnRandomRequests();

		} catch (Exception e) {
			logger.error("Sending back error 500.", e.getStackTrace().toString(), ctx, e);
			ctx.status(500);
		}
   }
}
