package app.health;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import app.directory.DirectoryService;
import kong.unirest.GetRequest;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;

public class HealthService {
	public static void spawnRandomRequests() throws InterruptedException {
		for (int j = 0; j < 10; j++) {
	        new Thread(() -> {
	            try {
	                for (int i = 0; i < new Random().nextInt(5); i++) {
	                    Unirest.get("http://localhost:8080/directory").asString();
	                }
	            } catch (UnirestException ignored) {
	            }
	        }).start();
	        Thread.sleep((int) (Math.random() * 250));
		}
    }

	public static Object create(int create) throws IOException {
		File testDir = new File("test");
		testDir.mkdir();

		 List<String> randomWords = randomWords();
		 Random r = new Random();
//		 System.out.println(randomWords.size());
		

		while(create > 0)
		{
			StringBuilder sb = new StringBuilder();
			for(int j = 0; j < ThreadLocalRandom.current().nextInt(1, 3); j++)
			{
				sb.append(randomWords.get(r.nextInt(0, randomWords.size())) + "/");
			}
			sb.append(randomWords.get(r.nextInt(0, randomWords.size()))).append(".").append(randomWords.get(r.nextInt(0, randomWords.size())));
			File childFile = new File(testDir, sb.toString());
//			System.out.println(childFile.getPath());
			if(!childFile.exists())
			{
				create--;
				if (!childFile.getParentFile().exists())
					childFile.getParentFile().mkdirs();
				childFile.createNewFile();
				
				if(create%10000 == 0)
					System.out.println("Files to be created: " + create);
			}
		}
		return DirectoryService.convertToDirectoryFile(testDir);
	}
	
	
	private static List<String> randomWords() throws IOException
	{
		String urlString = "http://random-word-api.herokuapp.com/all?lang=en";
		
			
		GetRequest getRequest = Unirest.get(urlString);
	
		
		String listString = getRequest.asString().getBody();
		
		listString = listString.replace("[","").replace("]","").replace("\"","");
		  

		List<String> list = new ArrayList<String>();
		list.addAll(Arrays.asList(listString.split(",")));
		
		return list;
		
		
	}
}
